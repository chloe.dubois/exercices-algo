// EX 1
// Ecrire un algorithme qui demande successivement 20 nombres à l’utilisateur, 
// et qui lui dise ensuite quel était le plus grand parmi ces 20 nombres :
// Modifiez ensuite l’algorithme pour que le programme affiche de surcroît 
// en quelle position avait été saisie ce nombre :
// C’était le nombre numéro 2

let div = document.getElementById('exo');

titre1 = document.createElement('h3');
titre1.innerText = 'Le plus grand nombre';
div.append(titre1);

let array = [];

for (let i = 0; i < 20; i += 1) {
    let number1 = prompt('Exo 1. Entrer un nombre');
    array.push(parseInt(number1));
}

let parag = document.createElement('p');
let parag2 = document.createElement('p');
let parag3 =  document.createElement('p');

let biggestNumber = Math.max(...array);
let numero = array.indexOf(biggestNumber) + 1;

parag.innerText = 'Le plus grand nombre que vous avez entré est ' + biggestNumber;
parag2.innerText = 'C\'était le nombre numéro ' + numero;
parag3.innerText = 'Nombres donnés : ' + array;

div.append(parag3,parag,parag2);

// EX2
// Ecrire un algorithme qui demande un nombre de départ, 
// et qui ensuite écrit la table de multiplication de ce nombre, 
// présentée comme suit (cas où l'utilisateur entre le nombre 7) :
// Table de 7 : 7 x 1 = 7 7 x 2 = 14 7 x 3 = 21 … 7 x 10 = 70

let number2 = prompt('Exo 2. Tables de multiplication. Entrer un nombre :');

titre2 = document.createElement('h3');
titre2.innerText = 'Table de ' + number2;
div.append(titre2);

for (let i = 1; i < 11; i += 1) {
    let parag4 = document.createElement('p');
    let multiplication = number2 * i;
    parag4.innerText = number2 + ' x ' + i + ' = ' + multiplication;
    div.append( parag4);
}

// EX 3
// Ecrire un algorithme qui demande un nombre de départ, et qui calcule sa factorielle.
// NB : la factorielle de 8, notée 8 !, vaut
// 1 x 2 x 3 x 4 x 5 x 6 x 7 x 8


let number3 = parseInt(prompt('Exo 3. Factorielles. Entrer un nombre :'));

titre3 = document.createElement('h3');
titre3.innerText = 'Factorielle de ' + number3;
div.append(titre3);

const Factorielle = (nb) => {   
    let fac = nb;
    for (let i = 1; i < fac ; i += 1) {
    nb = nb * i
    };
    return nb;
}

let result = Factorielle(number3);

let parag5 = document.createElement('p');
parag5.innerText = result ;
div.append( parag5);

// EX 4
// Écrire un algorithme qui permette de connaître ses chances de gagner au tiercé, 
// quarté, quinté et autres impôts volontaires.
// On demande à l’utilisateur le nombre de chevaux partants, et le nombre de chevaux joués. 
// Les deux messages affichés devront être :
// Dans l’ordre : une chance sur X de gagner 
// Dans le désordre : une chance sur Y de gagner
// X et Y nous sont donnés par la formule suivante, si n est le nombre de chevaux partants 
// et p le nombre de chevaux joués (on rappelle que le signe ! signifie "factorielle") :
// X = n ! / (n - p) ! Y = n ! / (p ! * (n – p) !)


titre4 = document.createElement('h3');
titre4.innerText = 'Vos chances de gagner : ';
div.append(titre4);

let n = parseInt(prompt('Exo 4. Chances de gagner. Nombre de chevaux partant :'));
let p = parseInt(prompt('Exo 5. Chances de gagner. Nombre de chevaux joués :'));

let X = Factorielle(n) / Factorielle(n - p);
let Y = Factorielle(n) / (Factorielle(p) * Factorielle(n - p));

let parag6 = document.createElement('p');
parag6.innerText = 'Dans l\'ordre : une chance sur ' + X + ' de gagner.';

let parag7 = document.createElement('p');
parag7.innerText = 'Dans le désordre : une chance sur ' + Y + ' de gagner.';
div.append(parag6, parag7);

// EX 5
// Faire un algorithme qui détermine la longueur d’une chaîne de caractères.

titre5 = document.createElement('h3');
titre5.innerText = 'Longueur d\'une chaine : ';
div.append(titre5);

let chaine = prompt('Exo 5. Entrer une chaine de caractères:');

let nbOfCarac = chaine.length;

let parag8 = document.createElement('p');
parag8.innerText = 'La chaine de caractère "' + chaine + '" comporte " ' + nbOfCarac+ ' caractères.';
div.append(parag8);

// EX 6
// Faire une fonction de concaténation (ajoute à la fin de la première chaîne de caractères 
// le contenu de la deuxième chaîne de caractères.)

titre6 = document.createElement('h3');
titre6.innerText = 'Chaines de caractères';
div.append(titre6);

let chaine2 = prompt('Exo 6. Entrer une chaine de caractères:');
let chaine3 = prompt('Exo 6. Entrer une deuxième chaine de caractères:');

let concat = chaine2 + chaine3;
let parag9 = document.createElement('p');
parag9.innerText = 'Chaine concaténée : ' + concat;
div.append(parag9);

// Faire une fonction de Comparaison qui compare deux chaînes de caractères suivant l’ordre lexicographique.

// Faire une fonction qui efface une partie de la chaîne en spécifiant une longueur 
// d’effacement et un indice à partir duquel il faut effacer.

let indiceStart = 1;
let length = 2;

let efface = chaine2.replace(chaine2.substr(indiceStart, length), '');
let parag10 = document.createElement('p');
parag10.innerText = 'Chaine après effacement : ' + efface;
div.append(parag10);


// EX 7
// Ecrire un algorithme qui affiche la moyenne d’une suite d’entiers



// EX 8
// Ecrire une fonction max3 qui retourne le maximum de trois entiers
// Ecrire une fonction min3 qui retourne le minimum de trois entiers
// Ecrire une fonction max2 qui retourne le maximum de deux entiers
// Ecrire une fonction max3 qui retourne le maximum de trois entiers en faisant appel à max2